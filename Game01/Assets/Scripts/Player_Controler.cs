﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player_Controler : MonoBehaviour {
	
	/////////movement
	public float movementSpeed=5.0f;
	public float mouseResponse=5.0f;
	public float UDRange = 60.0f;
	float verticalVelocity=0;
	float jumpspeed=5.0f;
	float verticalrotation = 0;
	bool ii=false;
	float forwardSpeed = 0 ;
	float sideSpeed =0;
	public float speed_cap = 25;
	public float friction_coeff = 0.9995f;
	//spawn rocket
	public float Rocketlaunch = 5f;
	public GameObject rocket_prefab;
	public float Rocketoffset = 5f;
	public float Rockets_per_sec = 1;
	public float Player_Rocket_mass_ratio = 1;
	float timer;
	bool Rocket_shooted = false;
	//explosion
	public float recoil_coeff;


	/////////Health///////////////
	float cachedY;
	public RectTransform HEalthChange;
	float HEalthState;
	float MaxValueHEalth;
	float MinValueHEalth;
	public int MaxHEalth;
	int CurrentHEalth;
	int currentHEalth {
		get{ return CurrentHEalth;}
		set{ CurrentHEalth = value;
			HandleHealth();}
	}
	public Text healthText;
	public Image visualHealth;
	public float coolDown;
	bool onCD;
	/////////////////////////////////

	//reflections from walls
	public bool reflect = false;
	public Vector3 normal_collision_vector;
	float reflect_counter = 0;
	
	CharacterController chacracterController;
	
	// Use this for initialization
	void Start () {
		Debug.Log ("I am alive");
		Screen.lockCursor = true;
		normal_collision_vector = new Vector3 (-1, 0, 0);
		/////////////////HEALTH/////////////////////////////////////////////////////////////////////////
		cachedY = HEalthChange.position.y;
		MaxValueHEalth = HEalthChange.position.x;
		MinValueHEalth = HEalthChange.position.x - HEalthChange.rect.width;
		CurrentHEalth = MaxHEalth;
		onCD = false;
		////////////////////////////////////////////////////////////////////////////////////////////////

	}
	
	// Update is called once per frame
	void Update () {
		chacracterController = GetComponent<CharacterController>();
		
		//Rotate
		float rotLR=Input.GetAxis ("Mouse X")* mouseResponse;
		transform.Rotate (0, rotLR, 0);
		
		
		verticalrotation -= Input.GetAxis ("Mouse Y")* mouseResponse;
		verticalrotation = Mathf.Clamp (verticalrotation, -UDRange, UDRange);
		Camera.main.transform.localRotation = Quaternion.Euler (verticalrotation, 0, 0);
		
		//Move
		
		
		float forwardSpeed_new = Input.GetAxis ("Vertical")*movementSpeed;
		float sideSpeed_new = Input.GetAxis ("Horizontal")*movementSpeed;
		
		
		
		verticalVelocity += Physics.gravity.y * Time.deltaTime;
		if((chacracterController.collisionFlags & CollisionFlags.Below) != 0)
			verticalVelocity = 0;
		
		//double jump
		if ( Input.GetButtonDown ("Jump")) {
			if((chacracterController.collisionFlags & CollisionFlags.Below) != 0 && !ii){
				verticalVelocity = jumpspeed;
				ii=true;
			}
			else if(ii==true){
				ii=false;
				verticalVelocity = jumpspeed;
			}
		}
		
		
		
		
		Vector3 speed_old = new Vector3 (sideSpeed, 0, forwardSpeed);
		
		Vector3 speed_new = new Vector3 (sideSpeed_new, verticalVelocity, forwardSpeed_new);
		
		Vector3 speed_new2 = transform.rotation * speed_new;

		speed_new = transform.rotation * speed_new + speed_old;


	

		if ((chacracterController.collisionFlags & CollisionFlags.Below) != 0 
		    & (chacracterController.collisionFlags & CollisionFlags.Sides) == 0){
			//touching floor and not touching sides

			sideSpeed += speed_new2.x;
			forwardSpeed += speed_new2.z;

		}else if((chacracterController.collisionFlags & CollisionFlags.Below) != 0 
		          & (chacracterController.collisionFlags & CollisionFlags.Sides) != 0){
			// touching sides and  touching floor
			sideSpeed = 0;
			forwardSpeed = 0;
		}else if ((chacracterController.collisionFlags & CollisionFlags.Below) == 0 
		          & (chacracterController.collisionFlags & CollisionFlags.Sides) != 0){
			//  touching sides and not touching floor
			sideSpeed = 0;
			forwardSpeed = 0;
		}else if((chacracterController.collisionFlags & CollisionFlags.Sides) != 0){
			sideSpeed = 0;
			forwardSpeed = 0;//not touching sides and not touching floor
		}





		//normalize speed
		//1 normalize components
		if (sideSpeed > speed_cap)
			sideSpeed = speed_cap;
		if (sideSpeed < -speed_cap)
			sideSpeed = -speed_cap;
		if (forwardSpeed > speed_cap)
			forwardSpeed = speed_cap;
		if (forwardSpeed < -speed_cap)
			forwardSpeed = -speed_cap;

		//2 normalize magnitude
		float mag = Mathf.Pow(1/2,Mathf.Pow(2,forwardSpeed) + Mathf.Pow(2,sideSpeed));
		if(mag > speed_cap){
				forwardSpeed = forwardSpeed/mag * speed_cap;
				sideSpeed = sideSpeed/mag * speed_cap;
		}

		chacracterController.Move (speed_new * Time.deltaTime);


		//rocket spawning		
		if (Input.GetButtonDown ("Fire1"))
			spawn_rocket (speed_new);



		//reflections
		if (reflect == true && reflect_counter == 0)
						reflect_player ();


		if (reflect_counter > 0)
			reflect_counter--;

		//clear Rocket_shooted flag after some time
		timer += Time.deltaTime;
		if (timer >= Rockets_per_sec) {
			Rocket_shooted = false;
		}


		//friction -- apply only if player is touching floor
		if ((chacracterController.collisionFlags & CollisionFlags.Below) != 0) {
						sideSpeed *= friction_coeff;
						forwardSpeed *= friction_coeff;
				}

	}

	void reflect_player()
	{
		reflect_counter = 3;
		reflect = false;
		Debug.Log ("wall!");
		Vector3 speed_update = new Vector3 (sideSpeed, 0, forwardSpeed);
		speed_update = Vector3.Reflect (speed_update, normal_collision_vector);
		sideSpeed = speed_update.x;
		verticalVelocity = speed_update.y;
		forwardSpeed = speed_update.z;

	}
	
	//spawn rocket
	void spawn_rocket(Vector3 speed_new){
		//rocket cap per sec

		
		if (Rocket_shooted == true)
						return;
		Camera cam = Camera.main;
		//Vector3 ninety_deg = new Vector3 (-90,180,90);
		Vector3 ninety_deg = new Vector3 (-0,0,0);

		GameObject Rocket = (GameObject)Instantiate(rocket_prefab,cam.transform.position/*DODANE->*/ +cam.transform.forward*Rocketoffset ,cam.transform.rotation);
		//action
		Rocket.rigidbody.AddForce(cam.transform.forward * Rocketlaunch+speed_new,ForceMode.Impulse);
		Rocket.transform.Rotate (ninety_deg);
		//reaction:
		sideSpeed -= cam.transform.forward.x * Rocketlaunch/Player_Rocket_mass_ratio;
		forwardSpeed -= cam.transform.forward.z * Rocketlaunch/Player_Rocket_mass_ratio;



		//rocket cap
		Rocket_shooted = true;
		timer = 0;
	}



	/////////Health///////////////////////////////////////////////////////////////

	IEnumerator CoolDownDmg(){
		onCD = true;
		yield return new WaitForSeconds (coolDown);
		onCD = false;
	}
	private void HandleHealth()	{
		healthText.text = "Health: " + CurrentHEalth;

		float currentXValue = MapValues (CurrentHEalth, 0, MaxHEalth, MinValueHEalth, MaxValueHEalth);

		HEalthChange.position = new Vector3 (currentXValue, cachedY);
		if (CurrentHEalth > MaxHEalth / 2) {
			visualHealth.color = new Color32((byte)MapValues(CurrentHEalth,MaxHEalth/2,MaxHEalth,255,0),255,0,255);
		}
		else {
			visualHealth.color = new Color32(255,(byte)MapValues(CurrentHEalth,0,MaxHEalth/2,0,255),0,255);
		}
	}
	private float MapValues(float x, float inMin,float inMax,float outMin,float outMax){
		return(x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
	}

	void OnTriggerStay(Collider other){
				if (other.name == "Damage") {
						if (!onCD && CurrentHEalth > 0) {
								StartCoroutine (CoolDownDmg ());
								currentHEalth -= 1;
						}
				}

				if (other.name == "Health") {
						if (!onCD && CurrentHEalth < MaxHEalth) {
								StartCoroutine (CoolDownDmg ());
								currentHEalth += 1;
						}
				}
		}
	void OnTriggerEnter(Collider other){///////////////////////////EXPLOSIONS!
		if (other.name == "Sphere" || other.name == "Sphere(Clone)") {
			SphereCollider Sphere = other.GetComponent<SphereCollider>();
			if (!onCD && CurrentHEalth > 0) {
				StartCoroutine (CoolDownDmg ());
				currentHEalth -= (int)(100/Sphere.radius); //ok working
			}
			//RECOIL
			Vector3 recoil = transform.position - other.transform.position;
			verticalVelocity+= recoil_coeff * recoil.y / Sphere.radius;
			forwardSpeed+= recoil_coeff * recoil.z / Sphere.radius;
			sideSpeed+= recoil_coeff * recoil.x / Sphere.radius;

		}

	}
	/////////////////////////////

	////////////////////////////////////////////////
}