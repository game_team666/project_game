﻿using UnityEngine;
using System.Collections;

public class SelfDestrucion : MonoBehaviour {
	public bool self_destruction;
	public float time;
	public float time_fire;
	public GameObject fire;
	public GameObject fire1;

	void DestroySphere()
	{
		Destroy (gameObject);

	}

	void DestroyFire()
	{
		Destroy (fire1);
	}
	void Update () {
	
		if (self_destruction == true) {
		     fire1=(GameObject)Instantiate(fire,transform.position,Quaternion.identity);
			Invoke("DestroySphere",time);
			Invoke("DestroyFire",time_fire);
			}
		self_destruction = false;
	}
}
