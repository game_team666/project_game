﻿using UnityEngine;
using System.Collections;

public class NetworkManager : MonoBehaviour {
	public GameObject StandbyCamera;

	SpawnSpot[] SPawnSPot ;
	// Use this for initialization
	void Start () {
		SPawnSPot = GameObject.FindObjectsOfType<SpawnSpot> ();
		Connect ();
	}

	void Connect(){
		PhotonNetwork.ConnectUsingSettings ("Rock v1");

	}
	void OnGUI(){
		GUILayout.Label (PhotonNetwork.connectionStateDetailed.ToString ());
	}
	void OnJoinedLobby(){
		PhotonNetwork.JoinRandomRoom ();
		}
	void OnPhotonRandomJoinFailed(){
		Debug.Log ("Join Failed");
		PhotonNetwork.CreateRoom (null);
		}
	void OnJoinedRoom()
	{
		Debug.Log ("On Join");
		SpawnMyPlayer ();
		}
	void SpawnMyPlayer()
	{
		SpawnSpot mySpawn = SPawnSPot [Random.Range (0, SPawnSPot.Length)];
		GameObject MYPLAYER = PhotonNetwork.Instantiate ("Player", mySpawn.transform.position, mySpawn.transform.rotation, 0);
		StandbyCamera.SetActive (false);
		((MonoBehaviour)MYPLAYER.GetComponent ("Player_Controler")).enabled = true;
		MYPLAYER.transform.FindChild ("Main Camera").gameObject.SetActive (true);
		}

}
